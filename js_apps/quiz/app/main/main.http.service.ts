import { BaseHttp } from '../utils';
import { Injectable } from '@angular/core'; 
 
@Injectable()
export class MainHttpService extends BaseHttp{  

    public saveRoom(data: any): any {
        var url = '/quiz/api/create/quiz';
        return this.makeRequest('post',url,data);
    }

}